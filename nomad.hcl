job "mongodb" {
    datacenters = ["dc1"]
    type = "service"
    group "mongodb" {
        network {
            port "db" {
                static = 27017
                to = 27017
            }
        }
        service {
            name = "mongodb"
            tags = ["db"]
            port = "db"
        }
        task "mongodb" {
			vault {
				policies = ["mongodb"]
			}
            resources {
                cpu = 500
                memory = 500
            }
			template {
				data        = <<EOH
{{ with secret "pki_int/cert/ca_chain" }}{{ .Data.certificate }}
{{ end }}
EOH
				destination = "local/cachain.pem"
                perms = 444
			}
			template {
				data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_db") }}
{{ with secret "pki_int/issue/cert" "role_name=mongodb" "common_name=mongodb.service.consul" "ttl=24h" "alt_names=_mongodb._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.certificate }}
{{ .Data.private_key }}
{{ end }}{{ end }}
EOH
				destination = "secrets/cert.pem"
                perms = 444
			}
            template {
                data = <<EOH
storage:
  dbPath: /data/db/
  journal:
    enabled: true
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log
security:
    authorization: enabled
net:
  port: 27017
  bindIp: 0.0.0.0
  tls:
    mode: requireTLS
    certificateKeyFile: /secrets/cert.pem
    CAFile: /local/cachain.pem
    allowConnectionsWithoutCertificates: true
EOH
                destination = "local/mongod.conf"
                perms = 444
            }
            driver = "docker"
            config {
                image = "mongo:4.2"
                ports = ["db"]
                args = ["--config", "/local/mongod.conf"]
            }
            volume_mount {
                volume = "data"
                destination = "/data/db"
            }
        }
        volume "data" {
            type = "host"
            source = "mongodb"
            read_only = false
        }
    }
}
